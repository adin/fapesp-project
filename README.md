[![build status](https://gitlab.com/adin/fapesp-project/badges/master/build.svg)](https://gitlab.com/adin/fapesp-project/commits/master)

# Notice

**This project is no longer maintained, and you should be using the general [`project-template`](https://gitlab.com/adin/project-template) instead.**

# About

This style formats an article to comply with FAPESP rules for formating a research project.

It provides an easy template to follow and it creates title pages in Portuguese and English.

# Install

## Clone

The repository should be cloned into your local `texmf` tree.

- In Unix systems:
```bash
mkdir -p ~/texmf/tex/latex/
cd ~/texmf/tex/latex/
git clone git@gitlab.com:adin/fapesp-project.git
```

- In macOS system (when latex is installed with mactex):
```bash
mkdir -p ~/Library/texmf/tex/latex/
cd ~/Library/texmf/tex/latex/
git clone git@gitlab.com:adin/fapesp-project.git
```

- In other operating systems it may vary depending on your installation. To discover the path you can
```bash
kpsewhich -var-value TEXMFHOME
```

## Download

Another option is to download the files and paste them in your local `texmf` tree.

Create a directory to store the package. In our case we will create `fapesp-project`

In Unix systems:
```bash
mkdir -p ~/texmf/tex/latex/fapesp-project
```

In macOS system (when latex is installed with mactex):
```bash
mkdir -p ~/Library/texmf/tex/latex/fapesp-project
```

Download the file `fapesp.sty`, and paste it in the previous folder.

# Use

Include the package in your main document with

```latex
\usepackage{fapesp}
```

For a more detail example, see [here](example/project.tex).